package lang.c;
import lang.*;

public class CSymbolTableEntry extends SymbolTableEntry {
    public  CType  type;
    public int    size;
    public boolean constp;
    public boolean isGlobal;
    public int address;

    public CSymbolTableEntry(CType type, int size, boolean constp, boolean isGlobal, int addr) {
        this.type     = type;// この識別子に対して宣言された型
        this.size     = size;// メモリ上に確保すべきワード数
        this.constp   = constp;// 定数宣言か?
        this.isGlobal = isGlobal;// 大域変数か?
        this.address  = addr;// 割り当て番地
    }

    public String toExplainString() {
        return type.toString() + ", "+ size + (constp ? "定数":"変数");
    }

}
