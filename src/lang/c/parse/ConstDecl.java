package lang.c.parse;

import java.awt.desktop.SystemSleepEvent;
import java.io.PrintStream;
import java.util.ArrayList;

import lang.*;
import lang.c.*;

public class ConstDecl extends CParseRule {
    private CToken const_,tk;
    private CParseRule constItem;
    private ArrayList<CParseRule> constItems = new ArrayList<CParseRule>();
    public ConstDecl(CParseContext pcx) {
    }
    public static boolean isFirst(CToken tk) {
        return tk.getType() == CToken.TK_CONST;
    }
    public void parse(CParseContext pcx) throws FatalErrorException {
        CTokenizer ct = pcx.getTokenizer();
        const_ = ct.getCurrentToken(pcx);// 'const'が取り出されるはず

        tk = ct.getNextToken(pcx);//'int'が取り出さる
        if (tk.getType() == CToken.TK_INT) {
           tk = ct.getNextToken(pcx);
        } else {
            pcx.fatalError(tk.toExplainString() + "'const'の後ろは'int'です");
        }

        if (ConstItem.isFirst(tk)) {
            constItem = new ConstItem(pcx);
            constItem.parse(pcx);
            constItems.add(constItem);
        } else {
            pcx.fatalError(tk.toExplainString() + "'int'の後ろはdeclItemです");
        }

        tk = ct.getCurrentToken(pcx);//, or ;が取り出される
        while (tk.getType() == CToken.TK_COMMA){
            tk = ct.getNextToken(pcx);//constItemが取り出さる
            if (ConstItem.isFirst(tk)) {
                constItem = new ConstItem(pcx);
                constItem.parse(pcx);
                constItems.add(constItem);
            } else {
                pcx.fatalError(tk.toExplainString() + "','の後ろはconstItemです");
            }
            tk = ct.getCurrentToken(pcx);
        }
        if (tk.getType() == CToken.TK_SEMI) {
            ct.getNextToken(pcx);
        } else {
            pcx.fatalError(tk.toExplainString() + "constItemの後ろは';'です");
        }
    }

    public void semanticCheck(CParseContext pcx) throws FatalErrorException {
        for(CParseRule c : constItems){
            if (c != null) { c.semanticCheck(pcx); }
        }
    }

    public void codeGen(CParseContext pcx) throws FatalErrorException {
        PrintStream o = pcx.getIOContext().getOutStream();
        o.println(";;; ConstDecl starts");
        for(CParseRule c : constItems){
            if (c != null) { c.codeGen(pcx); }
        }
        o.println(";;; ConstDecl completes");
    }
}
