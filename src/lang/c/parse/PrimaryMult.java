package lang.c.parse;

import java.io.PrintStream;

import lang.*;
import lang.c.*;

public class PrimaryMult extends CParseRule {
    private CToken mult,tk;
    private CParseRule variable;
    public PrimaryMult(CParseContext pcx) {
    }
    public static boolean isFirst(CToken tk) {
        return tk.getType() == CToken.TK_MULT;
    }
    public void parse(CParseContext pcx) throws FatalErrorException {
        // ここにやってくるときは、必ずisFirst()が満たされている
        CTokenizer ct = pcx.getTokenizer();
        mult = ct.getCurrentToken(pcx);
        tk = ct.getNextToken(pcx);
        if (Variable.isFirst(tk)) {
            variable = new Variable(pcx);
            variable.parse(pcx);
        } else {
            pcx.fatalError(tk.toExplainString() + "*の後ろはVariableです");
        }
    }

    public void semanticCheck(CParseContext pcx) throws FatalErrorException {
        if(variable != null){
            variable.semanticCheck(pcx);
            if(((Variable) variable).arrayIsNull()){//なんでキャストが必要なんだ？？
                if(variable.getCType() != CType.getCType(CType.T_pint)){
                    //pcx.fatalError(tk.toExplainString() + "*のあとの識別子はint*でなければいけません。(右辺と左辺の型違い)");
                    pcx.fatalError( tk.toExplainString() + "識別子の型が無効です。");
                }
            }
            this.setCType(CType.getCType(CType.T_int));
            this.setConstant(variable.isConstant());
        }
    }

    public void codeGen(CParseContext pcx) throws FatalErrorException {
        PrintStream o = pcx.getIOContext().getOutStream();
        if (variable != null) {
            variable.codeGen(pcx);
            o.println("\tMOV\t-(R6), R0\t; PrimaryMult: アドレスを取り出して、内容を参照して、積む<" + mult.toExplainString() + ">");
            o.println("\tMOV\t(R0), (R6)+\t; PrimaryMult:");
        }
    }
}