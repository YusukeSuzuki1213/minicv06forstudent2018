package lang.c.parse;

import java.io.PrintStream;

import lang.*;
import lang.c.*;

public class Variable extends CParseRule {
    private CParseRule ident, array;
    private CToken tk,ntk;
    public Variable(CParseContext pcx) {
    }
    public boolean arrayIsNull() { return array == null; }
    public static boolean isFirst(CToken tk) {
        return Ident.isFirst(tk);
    }
    public void parse(CParseContext pcx) throws FatalErrorException {
        CTokenizer ct = pcx.getTokenizer();
        tk = ct.getCurrentToken(pcx);
        ident = new Ident(pcx);
        ident.parse(pcx);

        ntk = ct.getCurrentToken(pcx);
        if (Array.isFirst(ntk)) {
            array = new Array(pcx);
            array.parse(pcx);
        }
    }

    public void semanticCheck(CParseContext pcx) throws FatalErrorException {
        if(ident != null){
            ident.semanticCheck(pcx);
            if(array == null) {
                if(ident.getCType() == CType.getCType(CType.T_aint) || ident.getCType() == CType.getCType(CType.T_paint)) {
                    //pcx.fatalError( tk.toExplainString() + "識別子は、int、int*でなければいけません。(識別子の型)");
                    pcx.fatalError( tk.toExplainString() + "識別子の型が無効です。");
                }
                this.setCType(ident.getCType());
            } else {
                //hoge[piyo]
                array.semanticCheck(pcx);
                if(ident.getCType() == CType.getCType(CType.T_int) || ident.getCType() == CType.getCType(CType.T_pint)) {
                    //pcx.fatalError( tk.toExplainString() + "識別子は、int[], int*[]でなけらばいけません。(識別子の型)");
                    pcx.fatalError( tk.toExplainString() + "識別子の型が無効です。");
                }
                else if (ident.getCType() == CType.getCType(CType.T_aint)) {
                    this.setCType( CType.getCType(CType.T_int) );
                } else if(ident.getCType() == CType.getCType(CType.T_paint)) {
                    this.setCType( CType.getCType(CType.T_pint) );
                }
            }
        }
        this.setConstant(ident.isConstant());
    }

    public void codeGen(CParseContext pcx) throws FatalErrorException {
        PrintStream o = pcx.getIOContext().getOutStream();
        o.println(";;; Variable starts");
        if(ident != null){ident.codeGen(pcx);}
        if(array != null){array.codeGen(pcx);}
        o.println(";;; Variable completes");
    }
}
