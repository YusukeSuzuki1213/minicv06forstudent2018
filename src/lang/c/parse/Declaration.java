package lang.c.parse;

import java.io.PrintStream;

import lang.*;
import lang.c.*;

public class Declaration extends CParseRule {
    private CParseRule declaration;
    public Declaration(CParseContext pcx) {
    }
    public static boolean isFirst(CToken tk) {
        return IntDecl.isFirst(tk) || ConstDecl.isFirst(tk);
    }
    public void parse(CParseContext pcx) throws FatalErrorException {
        CTokenizer ct = pcx.getTokenizer();
        CToken tk = ct.getCurrentToken(pcx);

        if (IntDecl.isFirst(tk)) {
            declaration = new IntDecl(pcx);
        } else if (ConstDecl.isFirst(tk)){
            declaration = new ConstDecl(pcx);
        } else {
            pcx.fatalError("declarationクラスでエラー");
        }
        declaration.parse(pcx);
    }

    public void semanticCheck(CParseContext pcx) throws FatalErrorException {
        declaration.semanticCheck(pcx);
    }

    public void codeGen(CParseContext pcx) throws FatalErrorException {
        PrintStream o = pcx.getIOContext().getOutStream();
        o.println(";;; declaration starts");
        declaration.codeGen(pcx);
        o.println(";;; declaration completes");
    }
}
